package exercise;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class PairedTag extends Tag {

    private String body;
    List<Tag> children;

    public PairedTag(String name, Map<String, String> attribues, String body, List<Tag> children) {
        this.name = name;
        this.attributes = attribues;
        this.body = body;
        this.children = children;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString());

        for (Tag child : children) {
            if (child instanceof SingleTag) {
                sb.append(child.toString());
            }
        }
        sb.append(body)
                .append("</")
                .append(name)
                .append(">");

        return sb.toString();
    }
}
// END

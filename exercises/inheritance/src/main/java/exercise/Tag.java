package exercise;

import java.util.stream.Collectors;
import java.util.Map;

// BEGIN
abstract class Tag {
    protected String name;
    protected Map<String, String> attributes;

    public String toString() {
        StringBuilder sb = new StringBuilder("<")
                .append(name);

        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            sb.append(" ")
                    .append(entry.getKey())
                    .append("=\"")
                    .append(entry.getValue())
                    .append("\"");
        }

        sb.append(">");
        return sb.toString();
    }
}
// END

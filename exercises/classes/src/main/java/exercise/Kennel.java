package exercise;

import java.util.Arrays;


// BEGIN
class Kennel {
    private static String[][] kennel = new String[100][2];
    private static int dogCount = 0;

    public static void addPuppy(String[] puppy) {
        kennel[dogCount] = puppy;
        dogCount++;
    }

    public static void addSomePuppies(String[][] puppies) {
        for(String[] puppy : puppies) {
            addPuppy(puppy);
        }
    }

    public static int getPuppyCount() {
        return dogCount;
    }

    public static boolean isContainPuppy(String puppy) {
        for(String[] kennelPuppy : kennel) {
            if (kennelPuppy[0].equals(puppy)) {
                return true;
            }
        }

        return false;
    }

    public static String[][] getAllPuppies() {
        String[][] result = new String[dogCount][2];
        System.arraycopy(kennel, 0, result, 0, dogCount);
        return result;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] breedPuppies = new String[dogCount];

        int it = 0;
        for(String[] puppy : kennel) {
            if (puppy[1].equals(breed)) {
                breedPuppies[it] = puppy[0];
                it++;
            }
        }

        String[] result = new String[it];
        System.arraycopy(breedPuppies, 0, result, 0, it);
        return result;
    }

    public static void resetKennel() {
        for (int i = 0; i < kennel.length; i++) {
            kennel[i] = new String[]{"", ""};
        }
        dogCount = 0;
    }
}
// END

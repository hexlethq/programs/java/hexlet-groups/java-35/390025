<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>

<!-- BEGIN -->
<!DOCTYPE html>
<html>
<body>
<table>
    <%List<Map<String, String>> users = (ArrayList<Map<String, String>>)request.getAttribute("users");
        for (Map<String, String> user : users) {%>
    <tr>
        <td><%=user.get("id")%></td>
        <td><a href=<%="/users/show?id=" + user.get("id")%> >
                <%=user.get("firstName") + " " + user.get("lastName")%>
        </td>
    </tr>
    <%}%>
</table>
</body>
</html>
<!-- END -->

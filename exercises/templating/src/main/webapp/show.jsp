<%@ page import="java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->

<!DOCTYPE html>
<html>
    <body>
        <ul>
            <% Map<String, String> user = (Map<String, String>)request.getAttribute("user");
            for (Map.Entry<String, String> prop : user.entrySet()) {%>
            <li><%=prop.getKey() + ": " + prop.getValue() %></li>
            <%}%>
        </ul>
        <p><a href=<%="/users/delete?id=" + user.get("id")%>>delete</p>
    </body>
</html>
<!-- END -->

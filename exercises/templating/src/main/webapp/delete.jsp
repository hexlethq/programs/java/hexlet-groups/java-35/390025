<%@ page import="java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<!DOCTYPE html>
<% Map<String, String> user = (Map<String, String>) request.getAttribute("user"); %>
<html>
    <body>
        <p>Are you sure, you want to delete user: <%=user.get("firstName") + " " +user.get("lastName")%> ?</p>
        <form action=/users/delete?id=<%=user.get("id")%> method=post>
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    </body>
</html>
<!-- END -->

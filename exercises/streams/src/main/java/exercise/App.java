package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
public class App {
    public static long getCountOfFreeEmails(List<String> emails) {
        return emails.stream()
                .filter(e -> e.matches(".*(gmail\\.com|yandex\\.ru|hotmail\\.com)"))
                .count();
    }
}
// END

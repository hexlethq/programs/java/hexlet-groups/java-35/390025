package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double a, double b, double angle) {
        double angleInRadians = angleToRadians(angle);
        return a * b * Math.sin(angleInRadians) / 2;
    }

    public static double angleToRadians(double angle) {
        return angle * Math.PI / 180.0;
    }

    public static void main(String[] args) {
        double square = getSquare(4, 5, 45);
        System.out.println(square);
    }
    // END
}

package exercise;

class Converter {
    // BEGIN
    public static int convert(int memoryAmount, String measure) {
        int result;
        switch (measure) {
            case "Kb":
                result = memoryAmount /  1024;
                break;
            case "b":
                result = memoryAmount * 1024;
                break;
            default:
                result = 0;
                break;
        }

        return result;
    }

    public static void main(String[] args) {
        
        // String res = (String) converter(10, "b");
        int res = convert(10, "b");
        // String res = (String) resInt;
        
        System.out.println("10 Kb = " + res + " b");
    }
    // END
}

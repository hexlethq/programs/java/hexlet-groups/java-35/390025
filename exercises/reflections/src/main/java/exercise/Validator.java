package exercise;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
// https://www.baeldung.com/java-reflection

public class Validator {
    public static List<String> validate(Address address) {
        List<String> nullFieldsWithNotNullAnnotation = new ArrayList<>();

        Field[] fields = address.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = getFieldValue(field, address);

            NotNull annotation = field.getAnnotation(NotNull.class);
            if (annotation != null && value == null) {
                nullFieldsWithNotNullAnnotation.add(field.getName());
            }
        }

        return nullFieldsWithNotNullAnnotation;
    }

    public static Map<String, List<String>> advancedValidate (Address address) {
        Map<String, List<String>> result = new HashMap<>();

        Field[] fields = address.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = getFieldValue(field, address);

            List<String> errors = new ArrayList<>();
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof NotNull) {
                    if (!(annotation == null) && value == null) {
                        errors.add("can not be null");
                    }

                } else if (annotation instanceof MinLength) {
                    String stringValue = value instanceof String ? (String) value : null;
                    if (stringValue != null) {
                        int minLen = ((MinLength) annotation).minLength();
                        if (stringValue.length() < minLen) {
                            errors.add("length less than " + minLen);
                        }
                    }

                }
            }
            if (errors.size() > 0) {
                result.put(field.getName(), errors);
            }
        }

        return result;
    }

    private static Object getFieldValue(Field field, Object instance) {
        Object value = null;

        try {
            value = field.get(instance);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return value;
    }
}
// END

package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {

        if (arr.length == 0) {
            return arr;
        }

        int[] result = new int[arr.length];
        int j = 0;

        for (int i = arr.length - 1; i >= 0; i--) {
            result[j] = arr[i];
            j++;
        }

        return result;
    }

    public static int mult(int[] arr) {
        int result = 1;

        for (int j : arr) {
            result *= j;
        }

        return result;
    }

    public static int[] flattenMatrix(int[][] matrix) {
        if (matrix.length == 0) {
            return new int[0];
        }

        int[] result = new int[matrix.length * matrix[0].length];
        int itter = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result[itter] = matrix[i][j];
                itter++;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[][] matrix1 = new int[0][0];
        int[] result1 = flattenMatrix(matrix1);
        System.out.println(Arrays.toString(result1)); // => []

        int[][] matrix2 = {
                {1, 2, 3},
                {4, 5, 6}
        };
        int[] result2 = flattenMatrix(matrix2);
        System.out.println(Arrays.toString(result2)); // => [1, 2, 3, 4, 5, 6]
    }
    // END
}

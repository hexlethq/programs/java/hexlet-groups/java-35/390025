package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
public class App {
    public static <T> List<Map<T, T>> findWhere(List<Map<T, T>> books, Map<T ,T> filter) {
        List<Map<T, T>> result = new ArrayList<>();

        for (Map<T, T> book : books) {

            int isBookMatched = 0;

            for (Map.Entry<T, T> bookParam : book.entrySet()) {
                T bookParamValue = bookParam.getValue();
                T filterValue = filter.get(bookParam.getKey());
                if (bookParamValue.equals(filterValue)) {
                    isBookMatched++;
                }
            }

            if (isBookMatched == filter.size()) {
                result.add(book);
            }
        }

        return result;
    }
}
//END

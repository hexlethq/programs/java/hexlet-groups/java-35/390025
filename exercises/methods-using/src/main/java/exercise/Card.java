package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String secured = "*".repeat(starsCount);
        String lastDigits = cardNumber.substring(12);
        System.out.println(secured + lastDigits);
        // END
    }
}

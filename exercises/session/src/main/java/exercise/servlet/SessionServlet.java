package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;

import java.util.HashMap;
import java.util.Map;

import static exercise.App.getUsers;
import exercise.Users;

public class SessionServlet extends HttpServlet {

    private Users users = getUsers();

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        if (request.getRequestURI().equals("/login")) {
            showLoginPage(request, response);
            return;
        }

        response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doPost(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        switch (request.getRequestURI()) {
            case "/login":
                login(request, response);
                break;
            case "/logout":
                logout(request, response);
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private void showLoginPage(HttpServletRequest request,
                               HttpServletResponse response)
                 throws IOException, ServletException {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
        requestDispatcher.forward(request, response);
    }

    private void login(HttpServletRequest request,
                               HttpServletResponse response)
                 throws IOException, ServletException {

        // BEGIN
        Map<String, String> authUser = new HashMap<>();
        authUser.put("email", request.getParameter("email"));
        authUser.put("password", request.getParameter("password"));
        String flash = "Неверные логин или пароль";

        Map<String, String> user = users.findByEmail(authUser.get("email"));
        if (user == null || !authUser.get("password").equals("password")) {
            response.setStatus(422);
            request.setAttribute("user", authUser);
            request.getSession().setAttribute("flash", flash);
            request.getRequestDispatcher("/login.jsp").forward(request, response);
            return;
        }

        flash = "Вы успешно вошли";
        request.getSession().setAttribute("flash", flash);
        request.getSession().setAttribute("userId", user.get("id"));

        request.getRequestDispatcher("/welcome.jsp").forward(request, response);
        // END
    }

    private void logout(HttpServletRequest request,
                               HttpServletResponse response)
                 throws IOException, ServletException {

        // BEGIN
        String flash = "Вы успешно вышли";
        request.getSession().setAttribute("flash", flash);
        request.getSession().removeAttribute("userId");

        request.getRequestDispatcher("/welcome.jsp").forward(request, response);
        // END
    }
}

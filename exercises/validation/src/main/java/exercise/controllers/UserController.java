package exercise.controllers;

import io.ebeaninternal.server.util.Str;
import io.javalin.http.Handler;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.javalin.core.validation.Validator;
import io.javalin.core.validation.ValidationError;
import io.javalin.core.validation.JavalinValidation;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

import exercise.domain.User;
import exercise.domain.query.QUser;

public final class UserController {

    public static Handler listUsers = ctx -> {

        List<User> users = new QUser()
            .orderBy()
                .id.asc()
            .findList();

        ctx.attribute("users", users);
        ctx.render("users/index.html");
    };

    public static Handler showUser = ctx -> {
        long id = ctx.pathParamAsClass("id", Long.class).getOrDefault(null);

        User user = new QUser()
            .id.equalTo(id)
            .findOne();

        ctx.attribute("user", user);
        ctx.render("users/show.html");
    };

    public static Handler newUser = ctx -> {

        ctx.attribute("errors", Map.of());
        ctx.attribute("user", Map.of());
        ctx.render("users/new.html");
    };

    public static Handler createUser = ctx -> {
        // BEGIN
        Validator<String> firstNameValidator = ctx.formParamAsClass("firstName", String.class)
                .check(value -> {
                    if (value != null) {
                        return  (value.length() > 0);
                    }
                    return false;
                }, "firstName cannot be empty");

        Validator<String> lastNameValidator = ctx.formParamAsClass("lastName", String.class)
                .check(Objects::nonNull, "lastName cannot be null")
                .check(value -> value.length() >= 4, "lastName cannot be less then 4 chars");

        Validator<String> emailValidator = ctx.formParamAsClass("email", String.class);
        emailValidator.check(value ->
                EmailValidator.getInstance().isValid(emailValidator.getStringValue()),
                "invalid email");

        Validator<String> passwordValidator = ctx.formParamAsClass("password", String.class)
                .check(value -> value.length() >= 4, "password can't be less then 4 chars")
                .check(StringUtils::isNumeric, "password must contains only digits");

        User user = new User(
                firstNameValidator.getStringValue(),
                lastNameValidator.getStringValue(),
                emailValidator.getStringValue(),
                passwordValidator.getStringValue()
        );

        Map<String, List<ValidationError<?>>> errors = JavalinValidation.collectErrors(
                firstNameValidator,
                lastNameValidator,
                emailValidator,
                passwordValidator
        );

        if (errors.size() > 0) {
            ctx.attribute("errors",errors);
            ctx.attribute("user", user);
            ctx.status(422).render("users/new.html");
            return;
        }

        user.save();
        ctx.sessionAttribute("flash", "Пользователь успешно создан");
        ctx.redirect("/users");

        // END
    };
}

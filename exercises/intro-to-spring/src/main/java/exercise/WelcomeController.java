package exercise;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// BEGIN
@RestController
public class WelcomeController {

    @GetMapping("/")
    public String root() {
        return "Welcome to Spring";
    }

    @GetMapping("/hello")
    public String welcome(@RequestParam(defaultValue = "World") String name) {
        return "Hello, " + name + "!";
    }

}
// END

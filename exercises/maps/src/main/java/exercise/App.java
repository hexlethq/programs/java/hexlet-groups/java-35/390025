package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static Map<String, Integer> getWordCount(String sentence) {
        Map<String, Integer> result = new HashMap<>();

        for(String word: sentence.split(" ")) {
            if (!word.equals("")) {
                int count = result.getOrDefault(word, 0);
                result.put(word, ++count);
            }
        }

        return result;
    }



    public static String toString(Map<String, Integer> wordCount) {
        if (wordCount.size() == 0) {
            return "{}";
        }

        StringBuilder sb = new StringBuilder("{");

        for (Map.Entry<String, Integer> entry : wordCount.entrySet()) {
            sb.append("\n  " + entry.getKey() + ": " + entry.getValue());
        }
        sb.append("\n}");
        return sb.toString();
    }
}
//END

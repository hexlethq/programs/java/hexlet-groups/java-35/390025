package exercise.repository;

import exercise.model.City;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {

    // BEGIN
    Optional<City> findCityById(long id);

    List<City> findCitiesByNameStartingWithIgnoreCase(String firstLetter);

    // @Query(value = "SELECT * FROM CITIES ORDER BY name", nativeQuery = true)
    List<City> findAllByOrderByName();
    // END
}

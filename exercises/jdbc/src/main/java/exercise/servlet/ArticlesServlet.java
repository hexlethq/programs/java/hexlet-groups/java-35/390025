package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import exercise.App;
import org.apache.commons.lang3.ArrayUtils;

import exercise.TemplateEngineUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;



public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                showArticles(request, response);
                break;
            default:
                showArticle(request, response);
                break;
        }
    }

    private void showArticles(HttpServletRequest request,
                          HttpServletResponse response)
            throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");

        // BEGIN
        String offsetParam = request.getParameter("page");
        int offset = offsetParam == null ? 1 : Integer.parseInt(offsetParam);
        request.setAttribute("page", offset);

        String articlesSql = App.getFileContent("articles.sql");
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(articlesSql);
            preparedStatement.setInt(1, offset);

            ResultSet resultSet = preparedStatement.executeQuery();
            List<Map<String, String>> articles = new ArrayList<>();
            while (resultSet.next()) {
                Map<String, String> article = new HashMap<>();
                article.put("id", resultSet.getString("id"));
                article.put("title", resultSet.getString("title"));
                articles.add(article);
            }
            request.setAttribute("articles", articles);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // END
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                         HttpServletResponse response)
                 throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        String id = getId(request);

        String articleSql = App.getFileContent("article.sql");
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(articleSql);
            preparedStatement.setInt(1, Integer.parseInt(id));

            ResultSet resultSet = preparedStatement.executeQuery();
            Map<String, String> article = new HashMap<>();
            while (resultSet.next()) {
                article.put("id", resultSet.getString("id"));
                article.put("title", resultSet.getString("title"));
                article.put("body", resultSet.getString("body"));
            }

            if (article.size() == 0) {
                response.sendError(404);
            }
            request.setAttribute("article", article);

            resultSet.close();
            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        // END
        TemplateEngineUtil.render("articles/show.html", request, response);
    }

}

package exercise;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReveredSequenceTest {

    private String testString = "abcdef";
    private ReversedSequence reversedSequence;

    @BeforeEach
    void prepare() {
        reversedSequence = new ReversedSequence(testString);
    }

    @Test
    void ReveredSequenceToStringTest() {
        assertEquals("fedcba", reversedSequence.toString());
    }

    @Test
    void ReveredSequenceCharAtTest() {
        assertEquals('d', reversedSequence.charAt(2));
    }

    @Test
    void ReveredSequenceLengthTest() {
        assertEquals(6, reversedSequence.length());
    }

    @Test
    void ReveredSequenceSubSequenceTest() {
        assertEquals("edc", reversedSequence.subSequence(1,4).toString());
    }
}

package exercise;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// BEGIN
public class ReversedSequence implements CharSequence{

    String seq;

    ReversedSequence(String str) {
        seq = new StringBuilder(str).reverse().toString();
    }

    @Override
    public int length() {
        return seq.length();
    }

    @Override
    public char charAt(int index) {
        return seq.toCharArray()[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {

        char[] chars = seq.toCharArray();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < chars.length; i++) {
            if (i >= start && i < end) {
                sb.append(chars[i]);
            }
        }

        return sb.toString();
    }

    public String toString() {
        return seq;
    }
}
// END

package exercise;

// BEGIN
interface Home {
    double getArea();

    default int compareTo(Home another) {
        double currentArea = getArea();
        double anotherArea = another.getArea();
        if (currentArea > anotherArea) {
            return 1;
        } else if (currentArea < anotherArea) {
            return -1;
        }
        return 0;
    }
}
// END

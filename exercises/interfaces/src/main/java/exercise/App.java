package exercise;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> estates, int outputNum) {
        return estates.stream()
                .sorted(Comparator.comparingDouble(Home::getArea))
                .limit(outputNum)
                .map(Object::toString)
                .toList();
    }
}
// END

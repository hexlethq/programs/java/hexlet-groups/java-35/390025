// BEGIN
package exercise;

import com.google.gson.Gson;

public class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");

//        String[] fruits = {"apple", "pear", "lemon"};
//        String fr = toJson(fruits); // => ["apple","pear","lemon"]
//        System.out.println(fr);

    }

    public static String toJson(String[] list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }
}
// END

package exercise;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String getForwardedVariables(String conf) {
        return Stream.of(conf.split("\n")) //get stream of rows
                .filter(row -> row.startsWith("environment")) // get only envs rows
                .flatMap(row -> Arrays.stream(row.split(","))) // split after ',' -> get each env
                .filter(row -> row.contains("X_FORWARDED_")) // get only X_FORWARDED_ envs
                .map(row -> row.replaceAll(".*X_FORWARDED_", "")) // remove x_f_ and all before
                .map(row -> row.replaceAll("\"", "")) // remove " char
                // .peek(System.out::println) // debug log
                .reduce((r1, r2) -> r1 + ',' + r2) // concat with comma between
                .orElse(null); // safe check
    }
}
//END

package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {
        if (arr.length == 0) {
            return -1;
        }
        int maxValue = Integer.MIN_VALUE;
        int maxIndex = -1;

        for(int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && arr[i] > maxValue) {
                maxValue = arr[i];
                maxIndex = i;
            }
        }

        return maxIndex;
    }

    public static int[] getElementsLessAverage(int[] arr) {
        if (arr.length == 0) {
            return arr;
        }

        int[] lessThanAvg = new int[arr.length]; // IS THERE A PROPER WAY ???
        int avg = calculateAverage(arr);

        int it = 0;
        for (int n : arr) {
            if (n <= avg) {
                lessThanAvg[it] = n;
                it++;
            }
        }

        int[] result = new int[it];
        System.arraycopy(lessThanAvg, 0, result, 0, it);
        return result;
    }

    public static int calculateAverage(int[] arr) {
        int avg = 0;
        for(int n : arr) {
            avg += n;
        }
        avg /= arr.length;

        return avg;
    }

//    public static void main(String[] args) {
//        int[] numbers2 = {0, 1, 2, 3, 4, 5, 10, 12};
//        int[] result2 = getElementsLessAverage(numbers2);
//        System.out.println(Arrays.toString(result2));
//    }
    // END
}

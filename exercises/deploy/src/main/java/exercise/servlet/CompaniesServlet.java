package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;
import static exercise.Data.getCompanies;

@WebServlet(name = "/companies")
public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        String searchParam = request.getParameter("search");
        boolean isFound = false;

        PrintWriter writer = response.getWriter();
        for (Object companyObj : getCompanies()) {
            String company = companyObj instanceof String ? (String) companyObj : null;

            if (searchParam != null) {
                if (company.contains(searchParam)) {
                    writer.write(company + "\n");
                    isFound = true;
                }
            } else {
                writer.write(company + "\n");
                isFound = true;
            }

        }

        if (!isFound) {
            writer.write("Companies not found");
        }

        writer.close();
        // END
    }
}

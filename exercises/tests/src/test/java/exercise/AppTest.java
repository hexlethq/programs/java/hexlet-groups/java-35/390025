package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        List<Integer> result =  App.take(numbers1, 2);
        List<Integer> expected = new ArrayList<>(Arrays.asList(1, 2));
        Assertions.assertEquals(expected, result);

        List<Integer> numbers2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        result = App.take(numbers2, 8);
//        expected = new ArrayList<>(Arrays.asList(7, 3, 10));
        Assertions.assertEquals(numbers2, result);
        // END
    }
}

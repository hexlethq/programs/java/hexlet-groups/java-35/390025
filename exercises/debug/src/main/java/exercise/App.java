package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        if (!checkIfTriangleExists(a, b, c)) {
            return "Треугольник не существует";
        } 

        if (a == b && b == c) {
            return "Равносторонний";
        } else if (a == b || b == c || a == c) {
            return "Равнобедренный";
        }

        return "Разносторонний";
    }

    public static boolean checkIfTriangleExists(int a, int b, int c) {
        return a + b > c && b + c > a && a + c > b;
    }

     public static void main(String[] args) {
         System.out.println(getTypeOfTriangle(1, 2, 7));
         System.out.println(getTypeOfTriangle(5, 6, 7));
         System.out.println(getTypeOfTriangle(1, -2, 7));
     }
    // END
}

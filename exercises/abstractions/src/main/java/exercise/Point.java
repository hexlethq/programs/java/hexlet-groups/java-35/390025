package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        return new int[]{x, y};
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] point) {
        return "(" + point[0] + ", " + point[1] + ")";
    }

    public static int getQuadrant(int[] point) {
        int x = point[0];
        int y = point[1];

        if (x == 0 || y == 0) {
            return 0;
        }

        if (x > 0 && y > 0) {
            return 1;
        } else if (x < 0 && y > 0) {
            return 2;
        } else if (x < 0 && y < 0) {
            return 3;
        } else {
            return 4;
        }
    }

    // Optional
    public static int[] getSymmetricalPointByX(int[] point) {
        return new int[]{point[0], -point[1]};
    }

    public static int calculateDistance(int[] p1, int[] p2) {
        double distance = Math.pow(p2[0] - p1[0], 2) + Math.pow(p2[1] - p1[1], 2);
        distance = Math.sqrt(distance);
        return (int) distance;
    }

    public static void main(String[] args) {
        int[] point = makePoint(-3, -6);
        int[] symmetricalPoint = getSymmetricalPointByX(point);
        System.out.println(Point.pointToString(symmetricalPoint)); // (-3, 6)

        int[] point1 = makePoint(0, 0);
        int[] point2 = makePoint(3, 4);
        System.out.println(Point.calculateDistance(point1, point2)); // 5
    }
    // END
}

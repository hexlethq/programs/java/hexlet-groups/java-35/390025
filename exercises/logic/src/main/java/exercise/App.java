package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = (number % 2 != 0) && (number >= 1001) ? true : false;
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        String result = number % 2 == 0 ? "yes" : "no";
        System.out.println(result);
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes >= 0 && minutes <= 14) {
            System.out.println("First");
        } else if (minutes <= 30) {
            System.out.println("Second");
        } else if (minutes <= 45) {
            System.out.println("Third");
        } else if (minutes <= 59) {
            System.out.println("Fourth");
        } 
        // END
    }
}

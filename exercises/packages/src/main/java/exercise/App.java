// BEGIN
package exercise;

import exercise.geometry.*;

public class App {
    public static double[] getMidpointOfSegment(double[][] segment) {
        double x = (segment[0][0] + segment[1][0]) / 2.0;
        double y = (segment[0][1] + segment[1][1]) / 2.0;
        return new double[]{x,y};
    }

    public static double[][] reverse(double[][] segment) {
        double[][] result = new double[2][2];

        result[0] = Segment.getEndPoint(segment).clone();
        result[1] = Segment.getBeginPoint(segment).clone();

        return result;
    }
}
// END

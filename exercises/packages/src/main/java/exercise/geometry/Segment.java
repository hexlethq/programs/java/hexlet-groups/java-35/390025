// BEGIN
package exercise.geometry;

public class Segment {
    public static double[][] makeSegment(double[] p1, double[] p2) {
        double[][] segment = new double[2][p1.length];
        segment[0] = p1;
        segment[1] = p2;
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        return segment[0];
    }

    public static double[] getEndPoint(double[][] segment) {
        return segment[1];
    }
}
// END

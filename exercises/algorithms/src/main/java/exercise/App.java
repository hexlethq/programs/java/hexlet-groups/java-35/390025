package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        int[] sorted = new int[arr.length];

        for(int i = 0; i < arr.length; i++) {
            for (int j = 1; j < arr.length- i; j++) {
                if (arr[j] < arr[j-1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j-1];
                    arr[j-1] = tmp;
                }

            }
        }

        return arr;
    }

    public static int[] selectionSort(int[] arr) {
        for(int i = 0; i < arr.length; i++) {
            int min_idx = i;
            for(int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[min_idx]) {
                    min_idx = j;
                }
            }

            if (min_idx != i) {
                int tmp = arr[i];
                arr[i] = arr[min_idx];
                arr[min_idx] = tmp;
            }
        }

        return arr;
    }

    public static void main(String[] args) {
        int[] numbers = {3, 10, 4, 3, 15, 22, 6, 9, 7};
        int[] sorted = selectionSort(numbers);
        System.out.println(Arrays.toString(sorted)); // => [3, 3, 4, 10]
    }
    // END
}

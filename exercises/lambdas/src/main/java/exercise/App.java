package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] image) {
        return Arrays.stream(image)
                .map(line -> Arrays.stream(line)
                        .flatMap(pxl -> Stream.of(pxl, pxl))
                        .toArray(String[]::new)
                )
                .flatMap(line -> Stream.of(line, line))
                .toArray(String[][]::new);
    }

    public static String[][] enlargeWithNestedLoops(String[][] image) {
        if (image.length == 0) {
            return image;
        }

        int enlargedColumnsNum = image.length * 2;
        int enlargedRowsNum = image[0].length * 2;
        String[][] enlarged = new String[enlargedColumnsNum][enlargedRowsNum];
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[0].length; j++) {
                enlarged[i * 2][j * 2] = image[i][j];
                enlarged[i * 2 + 1][j * 2] = image[i][j];
                enlarged[i * 2][j * 2 + 1] = image[i][j];
                enlarged[i * 2 + 1][j * 2 + 1] = image[i][j];
            }
        }
        return enlarged;
    }
}
// END

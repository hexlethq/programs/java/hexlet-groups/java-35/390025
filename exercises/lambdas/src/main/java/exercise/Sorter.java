package exercise;

import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.time.LocalDate;
import java.util.stream.Collectors;

// BEGIN
public class Sorter {
    public static List<String> takeOldestMans(List<Map<String, String>> ppl) {
        return ppl.stream()
                .filter(person -> person.get("gender").equals("male"))
                .sorted(Comparator.comparing(p -> p.get("birthday")))
                .map(person -> person.get("name"))
                .collect(Collectors.toList());
    }
}
// END

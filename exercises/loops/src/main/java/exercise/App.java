package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String result = "";
        boolean takeChar = true;

        for (int i = 0; i < str.length(); i++) {
            char symbol = str.charAt(i);

            if (takeChar && symbol != ' ') {
                result += Character.toUpperCase(symbol);
                takeChar = false;
            }

            if (symbol == ' ') {
                takeChar = true;
            }
        }

        return result; 
    }
    // END
}

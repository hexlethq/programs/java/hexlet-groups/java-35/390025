package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] strs) {
        if (strs.length == 0) {
            return "";
        }

        StringBuilder list = new StringBuilder("<ul>\n");
        for(String str : strs) {
            list.append("  <li>" + str + "</li>\n");
        }
        list.append("</ul>");

        return list.toString();
    }

    public static String getUsersByYear(String[][] userData, int year) {
        String[] filteredUsers = new String[userData.length];

        int it = 0;
        for (String[] user : userData) {
            LocalDate userYear = LocalDate.parse(user[1]);
            if (userYear.getYear() == year) {
                filteredUsers[it] = user[0];
                it++;
            }
        }

        String[] resultList = new String[it];
        System.arraycopy(filteredUsers, 0, resultList, 0, it);
        return buildList(resultList);
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
//    public static String getYoungestUser(String[][] users, String date) throws Exception {
//        // BEGIN
//
//        // END
//    }
}

package exercise;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

import java.util.List;
import java.util.ArrayList;

// https://refactoring.guru/ru/design-patterns/state
// BEGIN
public class TcpConnection {

    private Connection connectionState;
    private String ip;
    private int port;
    private List<String> buffer;

    TcpConnection(String ip, int port) {
        this.ip = ip;
        this.port = port;
        buffer = new ArrayList<>();
        connectionState = new Disconnected(this);
    }

    public String getCurrentState() {
        return connectionState.getState();
    }

    public void connect() {
        connectionState.connect();
    }

    public void disconnect() {
        connectionState.disconnect();
    }

    public void write(String data) {
        connectionState.write(data);
    }

    public void setState(Connection state) {
        this.connectionState = state;
    }

    // how to understand API for third devs
    public void writeToBuffer(String data) {
        buffer.add(data);
    }

}
// END

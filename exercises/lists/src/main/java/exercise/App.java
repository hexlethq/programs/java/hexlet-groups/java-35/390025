package exercise;

import java.time.temporal.ChronoField;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {
    public static boolean scrabble(String letters, String word) {
        if (letters.length() < word.length()) {
            return false;
        }

        List<Character> lettersList = getListOfCharsFromString(letters);
        List<Character> wordList = getListOfCharsFromString(word.toLowerCase());
        List<Character> wordFromLetters = new ArrayList<>();

        for (Character ch : wordList) {
            if (lettersList.remove(ch)) {
                wordFromLetters.add(ch);
            }
        }

        return wordFromLetters.equals(wordList);
    }

    public static List<Character> getListOfCharsFromString(String str) {
        List<Character> result = new ArrayList<>();
        for (char c : str.toCharArray()) {
            result.add(c);
        }

        return result;
    }
}
//END

package exercise;

import kong.unirest.HttpRequestWithBody;
import kong.unirest.MultipartBody;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;
import io.ebean.Transaction;

import exercise.domain.User;
import exercise.domain.query.QUser;

import java.util.HashMap;
import java.util.Map;

class AppTest {

    private static Javalin app;
    private static String baseUrl;
    private static Transaction transaction;

    // BEGIN
    @BeforeAll
    static void beforeAll() {
        app = App.getApp();
        app.start();

        int port = app.port();
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    static void afterAll() {
        app.stop();
    }
    // END

    // Хорошей практикой является запуск тестов с базой данных внутри транзакции.
    // Перед каждым тестом транзакция открывается,
    @BeforeEach
    void beforeEach() {
        transaction = DB.beginTransaction();
    }

    // А после окончания каждого теста транзакция откатывается
    // Таким образом после каждого теста база данных возвращается в исходное состояние,
    // каким оно было перед началом транзакции.
    // Благодаря этому тесты не влияют друг на друга
    @AfterEach
    void afterEach() {
        transaction.rollback();
    }

    @Test
    void testRoot() {
        HttpResponse<String> response = Unirest.get(baseUrl).asString();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testUsers() {

        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что на станице есть определенный текст
        assertThat(content).contains("Wendell Legros");
        assertThat(content).contains("Larry Powlowski");
    }

    @Test
    void testUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/5")
            .asString();
        String content = response.getBody();

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(content).contains("Rolando Larson");
        assertThat(content).contains("galen.hickle@yahoo.com");
    }

    @Test
    void testNewUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    // BEGIN
    @Test
    void testCreateUser_positive() {
        User expectedUser = new User("Aleksandr", "Shohin", "test@gmail.com", "12345");

        Map<String, Object> fields = new HashMap<>();
        fields.put("firstName", expectedUser.getFirstName());
        fields.put("lastName", expectedUser.getLastName());
        fields.put("password", expectedUser.getPassword());
        fields.put("email", expectedUser.getEmail());

        HttpResponse<String> request = Unirest.post(baseUrl + "/users").fields(fields).asString();
        assertThat(request.getStatus()).isEqualTo(302);

        User actualUser = new QUser()
                .lastName.equalTo(expectedUser.getLastName())
                .findOne();

        assertThat(actualUser).isNotNull();
        assertThat(actualUser.getFirstName()).isEqualTo(expectedUser.getFirstName());
        assertThat(actualUser.getLastName()).isEqualTo(expectedUser.getLastName());
        assertThat(actualUser.getEmail()).isEqualTo(expectedUser.getEmail());
        assertThat(actualUser.getPassword()).isEqualTo(expectedUser.getPassword());
    }

    @Test
    void testCreateUser_negative() {
        User expectedUser = new User("Aleksandr", "", "test@gmail.com", "12345");

        Map<String, Object> fields = new HashMap<>();
        fields.put("firstName", expectedUser.getFirstName());
        fields.put("lastName", expectedUser.getLastName()); // invalid param
        fields.put("password", expectedUser.getPassword());
        fields.put("email", expectedUser.getEmail());

        HttpResponse<String> request = Unirest.post(baseUrl + "/users").fields(fields).asString();
        assertThat(request.getStatus()).isEqualTo(422);

        String body = request.getBody();
        assertThat(body.contains("Фамилия не должна быть пустой")).isTrue();

        User actualUser = new QUser()
                .lastName.equalTo(expectedUser.getLastName())
                .findOne();

        assertThat(actualUser).isNull();
    }
    // END
}

package exercise.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
    private String firstName;
    private String lastName;
    private int id;
    private String email;

    public User() {
        super();
    }


    public User(String firstName, String lastName, int id, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String toHtml() {
        StringBuilder sb = new StringBuilder();

        sb.append("<ul>")
                .append("<li>id: ")
                .append(id)
                .append("</li><li>firstname: ")
                .append(firstName)
                .append("</li><li> lastname: ")
                .append(lastName)
                .append("</li><li> email: ")
                .append(email)
                .append("</li></ul>");

        return sb.toString();
    }
}

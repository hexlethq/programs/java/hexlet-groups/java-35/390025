package exercise.servlet;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.model.User;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        File f = new File("src/main/resources/users.json").getAbsoluteFile();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(f, new TypeReference<List<User>>(){});
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        StringBuilder sb = new StringBuilder("<table>");

        List<User> usersList = getUsers();
        for (User user : usersList) {
            sb.append("<tr>")
                    .append("<td>")
                    .append(user.getId())
                    .append("</td><td><a href=/users/")
                    .append(user.getId())
                    .append(">")
                    .append(user.getFullName())
                    .append("</a></td></tr>");
        }
        sb.append("</table>");

        PrintWriter writer = response.getWriter();
        writer.write(sb.toString());
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        List<User> usersList = getUsers();

        User user = null;
        try {
            user = getUserById(usersList, Integer.parseInt(id));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (user == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        PrintWriter out = response.getWriter();

        out.write("<table>" + user.toHtml() + "</table>");
        // END
    }

    private User getUserById(List<User> usersList, int id) {
        for (User user : usersList) {
            if (user.getId() == id) {
                return user;
            }
        }

        return null;
    }
}

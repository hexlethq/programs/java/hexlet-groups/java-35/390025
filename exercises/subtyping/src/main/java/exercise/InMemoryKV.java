package exercise;

import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;

// BEGIN
public class InMemoryKV implements KeyValueStorage{

    private Map<String, String> storage;

    InMemoryKV(Map<String, String> initMap) {
        storage = new HashMap<>();
        storage.putAll(initMap);
    }

    @Override
    public void set(String key, String value) {
        storage.put(key, value);
    }

    @Override
    public void unset(String key) {
        storage.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        return storage.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        Map<String, String> result = new HashMap<>(storage);
        return result;
    }
}
// END

package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
public class App {
    public static void swapKeyValue(KeyValueStorage storage) {

        Map<String, String> newStorage = new HashMap<>();
        for (Entry<String, String> entry : storage.toMap().entrySet()) {
            newStorage.put(entry.getValue(), entry.getKey());
        }

        for (Entry<String, String> entry : storage.toMap().entrySet()) {
            storage.unset(entry.getKey());
            String value = newStorage.get(entry.getValue());
            String key = entry.getValue();
            storage.set(key, value);
        }
    }
}
// END

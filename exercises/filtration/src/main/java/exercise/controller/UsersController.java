package exercise.controller;
import com.querydsl.core.BooleanBuilder;
import exercise.model.User;
import exercise.model.QUser;
import exercise.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicateBuilder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.criteria.CriteriaBuilder;

// Зависимости для самостоятельной работы
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import com.querydsl.core.types.Predicate;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    // BEGIN

    // @GetMapping(path = "")
    // public Iterable<User> getUsers(@RequestParam(required = false) String firstName,
    //                                @RequestParam(required = false) String lastName) {
    //
    //     BooleanBuilder where = new BooleanBuilder();
    //     if (firstName != null) {
    //         where.and(QUser.user.firstName.containsIgnoreCase(firstName));
    //     }
    //     if (lastName != null) {
    //         where.and(QUser.user.lastName.containsIgnoreCase(lastName));
    //     }
    //
    //     return userRepository.findAll(where);

    @GetMapping(path = "")
    public Iterable<User> getUser(@QuerydslPredicate(root = User.class)
                                  Predicate predicate) {

        return userRepository.findAll(predicate);
    }
    // END
}


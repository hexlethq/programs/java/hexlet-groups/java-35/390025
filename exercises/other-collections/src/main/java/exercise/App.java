package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static <T> LinkedHashMap<String, String> genDiff(Map<String, T> m1, Map<String, T> m2) {
        LinkedHashMap<String, String> diffMap = new LinkedHashMap<>();

        // concat maps
        Map<String, T> combined = Stream.of(m1, m2)
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (l, r) -> r));

        // iterate
        Stream.of(combined)
                .flatMap(map -> map.entrySet().stream())
                .forEach(e -> diffOnKey(e.getKey(), m1, m2, diffMap));

        return diffMap;
    }

    private static <T> void diffOnKey(String key, Map<String, T> m1, Map<String, T> m2, Map<String, String> diffMap) {
        if (m1.containsKey(key)) {
            // m1 has a key
            if (m2.containsKey(key)) {
                // m2 has a key
                if (m1.get(key).equals(m2.get(key))) {
                    diffMap.put(key, "unchanged");
                } else {
                    diffMap.put(key, "changed");
                }
            } else {
                // m1 has a key, m2 doesn't
                diffMap.put(key, "deleted");
            }
        } else if (m2.containsKey(key)) {
            diffMap.put(key, "added");
        }
    }
}
//END

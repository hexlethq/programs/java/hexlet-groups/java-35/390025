package exercise;

import java.lang.reflect.Proxy;

import exercise.calculator.Calculator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor{
    public final static Logger LOGGER = LoggerFactory.getLogger(CustomBeanPostProcessor.class);

    Map<Object, String> annotatedBeans = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            String loggingLevel = bean.getClass().getAnnotation(Inspect.class).level();
            annotatedBeans.put(bean, loggingLevel);
            // LOGGER.info(bean.getClass().getName() + " ADDED");
        }

        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        if (annotatedBeans.containsKey(bean)) {
            String level = annotatedBeans.get(bean);

            return Proxy.newProxyInstance(
                    bean.getClass().getClassLoader(),
                    bean.getClass().getInterfaces(),
                    (proxy, method, args) -> {
                         String log = "Was called method: " + method.getName()
                                + "() with arguments: " + Arrays.toString(args);

                         try {
                             LOGGER.getClass().getMethod(level, String.class).invoke(LOGGER, log); // handle
                         } catch (NoSuchMethodException e) {
                             LOGGER.error("[NoSuchMethodException]: wrong level name in [" + beanName + "] bean");
                         }

                         return  method.invoke(bean, args);
                    }
            );
        }

        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
// END
